// 1. Write a program that prints the numbers from 1 to 100. 
//But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
// For numbers which are multiples of both three and five print “FizzBuzz”.

for (var i = 1; i <= 100; i++) {
    if (i % 15 == 0) console.log("FizzBuzz");
    else if (i % 3 == 0) console.log("Fizz");
    else if (i % 5 == 0) console.log("Buzz");
    else console.log(i);
}


// 2. Write a program for following condition:
//    a) Pass argument (array) to function then return only array element
//    b)  Sort the array which is filtered in first cycle.
//    input: [ [2], 23, ‘dance’, true, [3, 5, 3], [65, 45] ]
//    output: [2, 3, 5, 45, 65] // sorted order


//    a) Pass argument (array) to function then return only array element
function Car(a, b, c) {
    console.log(a, b, c);
  }
  const CarModel = ['BMW', 'Gentra', 'Malibu'];
  Car(...CarModel);

//    b)  Sort the array which is filtered in first cycle.

  var mixArr = [ [2], 23,true, [3, 5, 3], [65, 45] ]
  function filterArr(mixArr) {
      for(let key of mixArr) {
      if (typeof key === "object") {
          console.log(key);
      }
      }
  }
  filterArr(mixArr);
