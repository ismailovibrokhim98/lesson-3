## Task 1

Create gitlab account
Create simple HTML webpage where you have to write:

a. Head title ( Linear colored )
b. Description ( Simple text )
c. Rectangle figure ( color red )

**Output**
![image.png](./image.png)

## Task 2

1. Write a program that is divisible by 3 and 5
2. Write a program that checks whether given number odd or even.
3. Write a program that get array as argument and sorts it by order // [1,2,3,4...]
4. Write a program that return unique set of array:
   Input:
   [
   {test: ['a', 'b', 'c', 'd']},
   {test: ['a', 'b', 'c']},
   {test: ['a', 'd']},
   {test: ['a', 'b', 'k', 'e', 'e']},
   ]

Output: [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]

5. Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false

## Task 3

1. Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

2. Write a program for following condition:
   a) Pass argument (array) to function then return only array element
   b) Sort the array which is filtered in first cycle.
   input: [ [2], 23, ‘dance’, true, [3, 5, 3], [65, 45] ]
   output: [2, 3, 5, 45, 65] // sorted order
